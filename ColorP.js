class ColorP{
	constructor(color,xPos,yPos){
		this.color=color;
		var graphics = new PIXI.Graphics();
		graphics.beginFill(color);
		graphics.drawCircle(xPos,yPos,40);
		graphics.endFill();
		graphics.interactive = true;
		graphics.buttonMode = true;
		graphics.on('pointerdown', DrawColorP);
		app.stage.addChild(graphics);
	}
	drawCircleP(xPos,yPos){
		var graphics = new PIXI.Graphics();
		graphics.beginFill(this.color);
		graphics.drawCircle(xPos,yPos,20);
		graphics.endFill();
		app.stage.addChild(graphics);

	}
}