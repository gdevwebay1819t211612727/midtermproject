const app = new PIXI.Application({
	"width":1500,
	"height":600,
	"view": document.getElementById("game_canvas"),
	"backgroundColor": 0xDEB887,
	"autoStart":true
});
var update = setInterval(Checker,200)
//Blue= 0x0000FF Red=0xFF0000 Yellow=0xFFFF00 Green=0x00FF00 Pink=0xFFC0CB 
//Orange=0xFFA500 Purple=0x800080 Brown=0xA52A2A 
var allColors = [0x0000FF,0xFF0000,0xFFFF00,0x00FF00,0xFFC0CB,0xFFA500,0x800080,0xA52A2A];
//var randomColorSelect = [0x0000FF,0xFF0000,0xFFFF00,0x00FF00];
//var backUpRC = [0x0000FF,0xFF0000,0xFFFF00,0x00FF00];
var randomColorSelect = [];
var backUpRC = [];
var isStillGoing = true;
//var guessedColor = [0x0000FF,0xFF0000,0x00FF00,0xFFFF00];
//var guessedColor = [0x00FF00,0x0000FF,0xFF0000,0xFFFF00];
var guessedColor = [];
var whatRound = 1;
RandomColorGenerator()
var whatSlot = 1;
var whatDecSlot = 1;
var nodeSlots = [
[50,50],[50,150],[50,250],[50,350],
[110,50],[110,150],[110,250],[110,350],
[170,50],[170,150],[170,250],[170,350],
[230,50],[230,150],[230,250],[230,350],
[290,50],[290,150],[290,250],[290,350],
[350,50],[350,150],[350,250],[350,350],
[410,50],[410,150],[410,250],[410,350],
[470,50],[470,150],[470,250],[470,350],
[530,50],[530,150],[530,250],[530,350],
[590,50],[590,150],[590,250],[590,350],
[650,50],[650,150],[650,250],[650,350],
[710,50],[710,150],[710,250],[710,350],
];
var decNodeSlots = [
[43,380],[53,390],[43,390],[53,380],
[103,380],[113,390],[103,390],[113,380],
[163,380],[173,390],[163,390],[173,380],
[223,380],[233,390],[223,390],[233,380],
[283,380],[293,390],[283,390],[293,380],
[343,380],[353,390],[343,390],[353,380],
[403,380],[413,390],[403,390],[413,380],
[463,380],[473,390],[463,390],[473,380],
[523,380],[533,390],[523,390],[533,380],
[583,380],[593,390],[583,390],[593,380],
[643,380],[653,390],[643,390],[653,380],
[703,380],[713,390],[703,390],[713,380],
];

var generateLayout = new GenerateBoard();
var decWhite = new ColorDW(0xFFFFFF);
var decBlack = new ColorDB(0x000000);
var red = new ColorR(0xFF0000,50,550);
var blue = new ColorB(0x0000FF,150,550);
var green = new ColorG(0x00FF00,250,550);
var violet = new ColorV(0x800080,350,550);
var yellow = new ColorY(0xFFFF00,450,550);
var brown = new ColorBro(0xA52A2A,550,550);
var pink = new ColorP(0xFFC0CB,650,550);
var orange = new ColorO(0xFFA500,750,550);



console.log("CorrectColor = "+randomColorSelect);
//Analyzer(randomColorSelect,guessedColor);
function Checker(){
	if(guessedColor.length==4)
	{
		Analyzer(randomColorSelect,guessedColor);
		console.log("called Analyzer");
	}
	else if(whatRound>=13&&isStillGoing)
	{
		GameOverFunc(0);
	}
}
function RandomColorGenerator()
{
	for(let a = 0 ; a<4;a++)
	{
		var randDec = Math.random() * allColors.length;
		var randInt = Math.floor(randDec);
		randomColorSelect.push(allColors[randInt]);
		backUpRC.push(allColors[randInt]);
		allColors.splice(randInt,1);
	}
}
function DrawColorR() {
	red.drawCircleR(nodeSlots[whatSlot-1][0],nodeSlots[whatSlot-1][1]);
	guessedColor.push(0xFF0000);
	console.log("Pushed = "+0xFF0000);
	console.log("guessedColor = "+guessedColor.length);
	whatSlot++;
}
function DrawColorB() {
	blue.drawCircleB(nodeSlots[whatSlot-1][0],nodeSlots[whatSlot-1][1]);
	guessedColor.push(0x0000FF);
	console.log("Pushed = "+0x0000FF);
	console.log("guessedColor = "+guessedColor.length);
	whatSlot++;
}
function DrawColorG() {
	green.drawCircleG(nodeSlots[whatSlot-1][0],nodeSlots[whatSlot-1][1]);
	guessedColor.push(0x00FF00);
	console.log("Pushed = "+0x00FF00);
	console.log("guessedColor = "+guessedColor.length);
	whatSlot++;
}
function DrawColorV() {
	violet.drawCircleV(nodeSlots[whatSlot-1][0],nodeSlots[whatSlot-1][1]);
	guessedColor.push(0x800080);
	console.log("Pushed = "+0x800080);
	console.log("guessedColor = "+guessedColor.length);
	whatSlot++;
}
function DrawColorY() {
	yellow.drawCircleY(nodeSlots[whatSlot-1][0],nodeSlots[whatSlot-1][1]);
	guessedColor.push(0xFFFF00);
	console.log("Pushed = "+0xFFFF00);
	console.log("guessedColor = "+guessedColor.length);
	whatSlot++;
}
function DrawColorBro() {
	brown.drawCircleBro(nodeSlots[whatSlot-1][0],nodeSlots[whatSlot-1][1]);
	guessedColor.push(0xA52A2A);
	console.log("Pushed = "+0xA52A2A);
	console.log("guessedColor = "+guessedColor.length);
	whatSlot++;
}
function DrawColorP() {
	pink.drawCircleP(nodeSlots[whatSlot-1][0],nodeSlots[whatSlot-1][1]);
	guessedColor.push(0xFFC0CB);
	console.log("Pushed = "+0xFFC0CB);
	console.log("guessedColor = "+guessedColor.length);
	whatSlot++;
}
function DrawColorO() {
	orange.drawCircleO(nodeSlots[whatSlot-1][0],nodeSlots[whatSlot-1][1]);
	guessedColor.push(0xFFA500);
	console.log("Pushed = "+0xFFA500);
	console.log("guessedColor = "+guessedColor.length);
	whatSlot++;
}

function Analyzer(array1,array2){
	var corColnOrd=0;
	var corCol=0;
	var corColInd = [];
	for(let i = 0 ;i<=3;i++)
	{
		if(array1[i]==array2[i])
		{
			console.log("checkcorandord : "+"Array1 = "+array1[i]+" Array2 = "+array2[i]);
			corColnOrd++;
			corColInd.push(i);
			console.log("What Ind = "+i); 

		}
	}
	if(corColInd.length>0)
	{

		console.log("Before splice : "+"Array1 = "+array1+" Array2 = "+array2);
		for(let j = corColInd.length-1;j>=0;j--)
		{
			console.log("Process : "+"Array1 = "+array1+" Array2 = "+array2);
			console.log("splicing = " + corColInd[j]);
			array1.splice(corColInd[j],1);
			array2.splice(corColInd[j],1);
		}
	}
	console.log("CorrectColAndOrdFinishSplice : "+"Array1 = "+array1+" Array2 = "+array2);
	for(let x = 0;x<=array1.length-1;x++)
	{
		for(let y = 0;y<=array2.length-1;y++)
		{
			console.log("Check : "+array1[x]+" == "+array2[y]);
			if(array1[x] == array2[y])
			{
				corCol++;
			}
		}
	}
	//if(corCol>0)
	//{
	//	for(let j = corCol;j>=0;j--)
	//	{
	//		console.log("Second Process : "+"Array1 = "+array1+" Array2 = "+array2);
	//		array1.splice(j,1);
	//		array2.splice(j,1);
	//	}
	//}
	console.log("CorrectColAndOrd = "+ corColnOrd);
	console.log("CorrectColor = "+ corCol);
	if(corColnOrd == 4)
	{
		GameOverFunc(1);
	}
	DecisionCircles(corColnOrd,corCol);
	array1.splice(0,array1.length);
	array2.splice(0,array2.length);

	console.log("array2 = "+ array2+" and "+ "array1 = "+array1);
}

function DecisionCircles(correctColorNorder,correctColor){

	var total = correctColorNorder+correctColor;
	var neededTotal = 4-total;

	for(let x = 0;x<correctColorNorder;x++)
	{
		decWhite.drawCircleDW(decNodeSlots[whatDecSlot-1][0],decNodeSlots[whatDecSlot-1][1]);
		whatDecSlot++;
	}
	for(let y = 0;y<correctColor;y++)
	{
		decBlack.drawCircleDB(decNodeSlots[whatDecSlot-1][0],decNodeSlots[whatDecSlot-1][1]);
		whatDecSlot++;
	}
	if(neededTotal>0)
	{
		for(let z = 0;z<neededTotal;z++)
		{
			whatDecSlot++;
		}
	}
	whatRound++;
	randomColorSelect = [backUpRC[0],backUpRC[1],backUpRC[2],backUpRC[3]];

}

function GameOverFunc(res){
	isStillGoing = false;
	var tex = new GameOver();
	if(res == 1)
	{
		tex.drawPanel(1);
	}
	else if(res == 0)
	{
		tex.drawPanel(0);
	}
}