class GameOver{
	constructor(){

	}
	drawPanel(result){
		if(result == 1)
		{

			var box = new PIXI.Graphics();
			box.beginFill(0x000000);
			box.drawRect(200,50,600,400);
			box.endFill();

			var style = new PIXI.TextStyle({
				fontFamily: 'Arial',
    			fontSize: 45,
    			fill: 0xFFFFFF
			});

			var text = new PIXI.Text('You Won!!!!',style);
			text.x = 400;
			text.y = 200;

			app.stage.addChild(box);
			app.stage.addChild(text);
		}
		else if(result == 0)
		{
			var box = new PIXI.Graphics();
			box.beginFill(0x000000);
			box.drawRect(200,50,600,400);
			box.endFill();

			var style = new PIXI.TextStyle({
				fontFamily: 'Arial',
    			fontSize: 45,
    			fill: 0xFFFFFF
			});

			var text = new PIXI.Text('You Lose:((((',style);
			text.x = 400;
			text.y = 200;

			app.stage.addChild(box);
			app.stage.addChild(text);
		}
	}
}