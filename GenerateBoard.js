class GenerateBoard{
	constructor(){
		var graphics = new PIXI.Graphics();

		//GuessCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(900,50,30);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(900,150,30);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(900,250,30);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(900,350,30);
		graphics.endFill();

		//1stRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(50,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(50,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(50,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(50,350,20);
		graphics.endFill();
		//1stDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(43,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(53,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(43,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(53,380,5);
		graphics.endFill();
		//2ndRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(110,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(110,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(110,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(110,350,20);
		graphics.endFill();
		//2ndDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(103,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(113,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(103,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(113,380,5);
		graphics.endFill();
		//3rdRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(170,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(170,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(170,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(170,350,20);
		graphics.endFill();
		//3rdDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(163,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(173,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(163,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(173,380,5);
		graphics.endFill();
		//4thRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(230,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(230,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(230,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(230,350,20);
		graphics.endFill();
		//4thDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(223,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(233,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(223,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(233,380,5);
		graphics.endFill();
		//5thRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(290,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(290,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(290,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(290,350,20);
		graphics.endFill();
		//5thDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(283,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(293,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(283,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(293,380,5);
		graphics.endFill();
		//6thRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(350,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(350,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(350,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(350,350,20);
		graphics.endFill();
		//6thDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(343,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(353,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(343,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(353,380,5);
		graphics.endFill();
		//7thRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(410,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(410,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(410,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(410,350,20);
		graphics.endFill();
		//7thDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(403,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(413,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(403,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(413,380,5);
		graphics.endFill();
		//8thRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(470,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(470,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(470,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(470,350,20);
		graphics.endFill();
		//8thDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(463,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(473,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(463,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(473,380,5);
		graphics.endFill();
		//9thRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(530,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(530,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(530,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(530,350,20);
		graphics.endFill();
		//9thDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(523,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(533,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(523,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(533,380,5);
		graphics.endFill();
		//10thRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(590,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(590,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(590,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(590,350,20);
		graphics.endFill();
		//10thDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(583,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(593,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(583,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(593,380,5);
		graphics.endFill();
		//11thRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(650,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(650,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(650,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(650,350,20);
		graphics.endFill();
		//11thDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(643,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(653,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(643,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(653,380,5);
		graphics.endFill();
		//12thRound
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(710,50,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(710,150,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(710,250,20);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(710,350,20);
		graphics.endFill();
		//12thDecCircles
		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(703,380,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(713,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(703,390,5);
		graphics.endFill();

		graphics.beginFill(0xA9A9A9);
		graphics.drawCircle(713,380,5);
		graphics.endFill();

		app.stage.addChild(graphics);
	}
}