class ColorDB{
	constructor(color){
		this.color=color;
	}
	drawCircleDB(xPos,yPos){
		var graphics = new PIXI.Graphics();
		graphics.beginFill(this.color);
		graphics.drawCircle(xPos,yPos,5);
		graphics.endFill();
		app.stage.addChild(graphics);

	}
}